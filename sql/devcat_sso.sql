-- MySQL dump 10.13  Distrib 8.0.28, for macos11 (arm64)
--
-- Host: localhost    Database: devcat_sso
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `devcat_api_pattern`
--

DROP TABLE IF EXISTS `devcat_api_pattern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_api_pattern` (
  `id` char(32) NOT NULL COMMENT '涓婚敭ID',
  `api_pattern` varchar(255) NOT NULL COMMENT 'URL 鍖归厤妯″紡',
  `api_describe` varchar(255) NOT NULL COMMENT 'URL 鎻忚堪',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '鏄惁鍚敤',
  `create_time` datetime NOT NULL COMMENT '鍒涘缓鏃堕棿',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_api_pattern`
--

LOCK TABLES `devcat_api_pattern` WRITE;
/*!40000 ALTER TABLE `devcat_api_pattern` DISABLE KEYS */;
INSERT INTO `devcat_api_pattern` VALUES ('0853de0be54a082d25db3c341c2471c4','/test/**','测试',0,'2023-02-12 20:59:29');
/*!40000 ALTER TABLE `devcat_api_pattern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_note`
--

DROP TABLE IF EXISTS `devcat_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_note` (
  `id` int NOT NULL AUTO_INCREMENT,
  `note_title` varchar(100) NOT NULL,
  `note_describe` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `user_id` char(32) NOT NULL,
  `note_type` varchar(100) NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `view_status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'private',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_note`
--

LOCK TABLES `devcat_note` WRITE;
/*!40000 ALTER TABLE `devcat_note` DISABLE KEYS */;
INSERT INTO `devcat_note` VALUES (7,'第一篇笔记',NULL,'a1df6b81530b4055b9d20b86c7178f13','md','2023-02-09 20:49:59','2023-02-09 20:49:59','private'),(8,'Vert.x 中的路由处理的同步方式',NULL,'a1df6b81530b4055b9d20b86c7178f13','md','2023-02-09 20:51:03','2023-02-09 20:51:03','private');
/*!40000 ALTER TABLE `devcat_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_role`
--

DROP TABLE IF EXISTS `devcat_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_role` (
  `id` char(32) NOT NULL COMMENT '角色ID',
  `role_code` varchar(100) NOT NULL COMMENT '角色代码',
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Devcat 角色信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_role`
--

LOCK TABLES `devcat_role` WRITE;
/*!40000 ALTER TABLE `devcat_role` DISABLE KEYS */;
INSERT INTO `devcat_role` VALUES ('0513aeb830a411ed9b978c164579534f','ROLE_ADMIN','管理员账号','2022-09-10 09:00:53'),('15cca31232a011ed9b978c164579534f','ROLE_ANONYMOUS','匿名用户','2022-09-12 21:37:46'),('3a71eabb8ff84fbb833977dc1dcf24f3','ROLE_USER','普通用户','2022-09-07 22:25:00');
/*!40000 ALTER TABLE `devcat_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_system_log`
--

DROP TABLE IF EXISTS `devcat_system_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_system_log` (
  `id` char(32) NOT NULL COMMENT 'ID',
  `log_describe` varchar(255) DEFAULT NULL COMMENT '日志描述',
  `log_api` varchar(255) DEFAULT NULL COMMENT '日志接口',
  `req_username` varchar(255) DEFAULT NULL COMMENT '请求用户',
  `req_param` json DEFAULT NULL COMMENT '请求参数',
  `ip_address` varchar(25) DEFAULT NULL COMMENT 'IP地址',
  `ip_address_local` varchar(255) DEFAULT NULL COMMENT 'IP地址位置',
  `req_time` int DEFAULT NULL COMMENT '请求耗时',
  `req_status` tinyint(1) DEFAULT NULL COMMENT '请求状态',
  `error_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '错误日志',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_system_log`
--

LOCK TABLES `devcat_system_log` WRITE;
/*!40000 ALTER TABLE `devcat_system_log` DISABLE KEYS */;
INSERT INTO `devcat_system_log` VALUES ('657a59e4e560fb27634197e3b418bfbd','增加API接口配置','/sso/apiPattern/add','admin','{\"apiPattern\": {\"id\": \"fa605a443fbc21f2e46db5e57c093ebf\", \"enabled\": false, \"roleList\": [{\"id\": \"15cca31232a011ed9b978c164579534f\"}], \"apiPattern\": \"/test/**\", \"createTime\": \"2023-02-12 21:06:16.520674\", \"apiDescribe\": \"测试\"}}','0:0:0:0:0:0:0:1','本机地址',5,1,NULL,'2023-02-12 21:06:17');
/*!40000 ALTER TABLE `devcat_system_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_task`
--

DROP TABLE IF EXISTS `devcat_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `task_title` varchar(100) NOT NULL COMMENT '任务标题',
  `task_describe` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `task_start_time` datetime NOT NULL COMMENT '任务开始时间',
  `task_end_time` datetime NOT NULL COMMENT '任务结束时间',
  `complete_progress` int NOT NULL DEFAULT '0',
  `task_status` varchar(20) DEFAULT 'no_started' COMMENT '任务进度',
  `project_id` int NOT NULL COMMENT '项目ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `devcat_task_project_id_IDX` (`project_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_task`
--

LOCK TABLES `devcat_task` WRITE;
/*!40000 ALTER TABLE `devcat_task` DISABLE KEYS */;
INSERT INTO `devcat_task` VALUES (1,'测试任务','测试项目下的任务','2023-02-04 16:00:00','2023-02-22 16:00:00',0,'no_started',1,'2023-02-05 19:53:37'),(2,'完善任务列表功能','完成任务列表的删除、编辑和快捷修改任务状态的功能','2023-02-06 18:00:00','2023-02-06 19:00:00',0,'no_started',2,'2023-02-05 20:41:35'),(3,'完成任务状态的切换和筛选','完成任务状态的切换和筛选','2023-02-07 22:04:36','2023-02-09 19:00:00',0,'no_started',1,'2023-02-07 22:04:52');
/*!40000 ALTER TABLE `devcat_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_task_project`
--

DROP TABLE IF EXISTS `devcat_task_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_task_project` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `project_describe` varchar(255) DEFAULT NULL COMMENT '项目描述',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='任务所属项目';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_task_project`
--

LOCK TABLES `devcat_task_project` WRITE;
/*!40000 ALTER TABLE `devcat_task_project` DISABLE KEYS */;
INSERT INTO `devcat_task_project` VALUES (1,'测试项目','测试项目描述','2023-02-05 18:03:07'),(2,'SSO登录系统','用于登录认证和授权使用','2023-02-05 19:15:32');
/*!40000 ALTER TABLE `devcat_task_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_user`
--

DROP TABLE IF EXISTS `devcat_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_user` (
  `id` char(32) NOT NULL COMMENT '用户id',
  `username` char(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `email` varchar(100) NOT NULL COMMENT '用户邮箱',
  `email_status` tinyint(1) NOT NULL COMMENT '邮件验证状态',
  `account_non_expired` tinyint(1) NOT NULL COMMENT '账号是否没有过期',
  `account_non_locked` tinyint(1) NOT NULL COMMENT '账号是否没有锁定',
  `credentials_non_expired` tinyint(1) NOT NULL COMMENT '密码是否没有过期',
  `enabled` tinyint(1) NOT NULL COMMENT '用户是否启用',
  `create_time` datetime NOT NULL COMMENT '用户创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Devcat 用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_user`
--

LOCK TABLES `devcat_user` WRITE;
/*!40000 ALTER TABLE `devcat_user` DISABLE KEYS */;
INSERT INTO `devcat_user` VALUES ('0da09bff1af8b0b3f455d590ae34edd9','huhailong6','{bcrypt}$2a$10$YQNudA1aaQNpZMVNqWjbgujSDDrUbYo2Ejs3sOdKxcAcVcY6AzVgi','hhlworksapce@qq.com',0,1,1,1,1,'2023-02-04 22:01:48'),('4e109d74aa4eeeaae9725d0b23bb3e7e','yumingyang3','{bcrypt}$2a$10$ZYZJoZgBe/mDxHtuw6PaCuvwI5X8LolKlF142Qkko/MEDayJzshBW','yuming.yang@bj-tct.com',0,1,1,1,1,'2023-02-04 16:12:56'),('52ffdec4d75c668ae3bb137eca912c9e','zhaoxiaochun','{bcrypt}$2a$10$kMJNmM/ZvZ3jzmojGtotse52zMEBhVxrrVdTCxmoX0w.ZCDL7hsGm','zxc@devcat.cn',0,1,1,1,1,'2022-09-22 21:38:52'),('70ef1956047a6abe072ae067bc52a36c','yumingyang2','{bcrypt}$2a$10$U5br2Jfbxrhofas.gglp5eiB1CurYQXpPkxzPOV/iZvLQHdYFuuz2','yuming.yang@bj-tct.com',0,1,1,1,1,'2023-02-04 16:12:20'),('8884873a372809eccdcbd4811f15e4ea','huhailong5','{bcrypt}$2a$10$uvfn76bFFGr6EQQur0P5ces2Uq3fD.b/aNw82jfYUhBH7N20aqnGq','hhlworkspace@qq.com',0,1,1,1,1,'2023-02-04 21:59:40'),('a1df6b81530b4055b9d20b86c7178f13','huhailong','{bcrypt}$2a$10$9qGoYywbVi1ZKsToRjKUW.0h0AA7npDj9VGzfZlS8QzSeTX0Qvbyq','hhlworkspace@qq.com',1,1,1,1,1,'2022-09-07 22:19:00'),('d0793ca178715c5492128cc6599f99a7','huhailong3','{bcrypt}$2a$10$C4fpTSCLV4dRH1T3dtNdlOkzBjrB7bTohzOqk.1ZtYLBCwMnVLsoS','hhlworkspace@qq.com',0,1,1,1,1,'2023-02-04 16:15:07'),('d465dac608ab81abef33712ee01eacb9','huhailong4','{bcrypt}$2a$10$4Pi84OvYmvIQ4ibYx1CtpO9bymF/8Mh/mVD/hOJkWq50XfHVCp2/K','hhlworkspace@qq.com',0,1,1,1,1,'2023-02-04 21:55:56'),('d660f3abc1637360e93a4c2dce18c06c','admin','{bcrypt}$2a$10$.SEGeZa0kXcSvhveeagIG.9f1vd3ifA.SR8zYG9VRENz.B607nhTG','1976884704@qq.com',1,1,1,1,1,'2022-09-22 21:18:03'),('eead9837d09147a891c43f069761a1d3','huhailong2','{bcrypt}$2a$10$q6cDMokH0daN/4h25MfAreZPffGFS6IRGXZkBv5TExxw4bfaW/LpO','hhlworkspace@qq.com',1,0,1,1,1,'2023-02-04 16:14:00'),('f38e882c540e8c9d65d2038902539871','huhailong1','{bcrypt}$2a$10$UA0S.GTuvl3s9eA/9x7c6eJNtTDReHVlgSSoSo2abXKJ28JITIdD2','test@devcat.cn',0,1,1,1,1,'2022-10-05 17:53:25'),('fea53fd0adb86dab63425ecc940fefcb','yumingyang','{bcrypt}$2a$10$hniNpY8tGS1VxzGcAQ3osOSLgxznTgBvyFRp5pEbNif2KSVXRP13a','yuming.yang@bj-tct.com',0,1,1,1,1,'2023-02-04 16:12:04');
/*!40000 ALTER TABLE `devcat_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devcat_user_info`
--

DROP TABLE IF EXISTS `devcat_user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devcat_user_info` (
  `id` char(32) NOT NULL COMMENT 'ID',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别',
  `age` int DEFAULT NULL COMMENT '年龄',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户头像',
  `birthday` date DEFAULT NULL COMMENT '出生年月',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devcat_user_info`
--

LOCK TABLES `devcat_user_info` WRITE;
/*!40000 ALTER TABLE `devcat_user_info` DISABLE KEYS */;
INSERT INTO `devcat_user_info` VALUES ('0da09bff1af8b0b3f455d590ae34edd9','huhailong6',3,NULL,NULL,NULL,'huhailong6'),('4e109d74aa4eeeaae9725d0b23bb3e7e','yumingyang3',3,NULL,NULL,NULL,'yumingyang3'),('52ffdec4d75c668ae3bb137eca912c9e','zhaoxiaochun',3,NULL,NULL,NULL,'zhaoxiaochun'),('70ef1956047a6abe072ae067bc52a36c','yumingyang2',3,NULL,NULL,NULL,'yumingyang2'),('8884873a372809eccdcbd4811f15e4ea','huhailong5',3,NULL,NULL,NULL,'huhailong5'),('a1df6b81530b4055b9d20b86c7178f13','胡海龙',0,25,'/Users/huhailong/Documents/devcat_sso/user_avatars/user_avatars/huhailong.png','1996-11-21','huhailong'),('d0793ca178715c5492128cc6599f99a7','huhailong3',3,NULL,NULL,NULL,'huhailong3'),('d465dac608ab81abef33712ee01eacb9','huhailong4',3,NULL,NULL,NULL,'huhailong4'),('d660f3abc1637360e93a4c2dce18c06c','管理员',0,26,'/Users/huhailong/Documents/devcat_sso/user_avatars/admin.png','1995-10-11','admin'),('eead9837d09147a891c43f069761a1d3','huhailong2',3,NULL,NULL,NULL,'huhailong2'),('f38e882c540e8c9d65d2038902539871','huhailong1',3,NULL,NULL,NULL,'huhailong1'),('fea53fd0adb86dab63425ecc940fefcb','yumingyang',3,NULL,NULL,NULL,'yumingyang');
/*!40000 ALTER TABLE `devcat_user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note_tag`
--

DROP TABLE IF EXISTS `note_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `note_tag` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(100) DEFAULT NULL,
  `user_id` char(32) NOT NULL,
  `quote_count` int NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_tag`
--

LOCK TABLES `note_tag` WRITE;
/*!40000 ALTER TABLE `note_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `note_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note_text`
--

DROP TABLE IF EXISTS `note_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `note_text` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_text`
--

LOCK TABLES `note_text` WRITE;
/*!40000 ALTER TABLE `note_text` DISABLE KEYS */;
INSERT INTO `note_text` VALUES (7,'# 第一篇笔记'),(8,'# Hello Smart Cat\n\nThis is test page, My name is huhailong!\n\n~~~java\npublic class Test {\n	public static void main(String[]args){\n		System.out.println(\"Hello Smart Cat\");\n	}\n}\n~~~\n\n~~~python\nimport sys\n\nprint(\"Hello Smart Cat\")\n~~~\n\n~~~js\nfunction test(){\n	console.log(\"Hello Smart Cat\")\n}\n~~~');
/*!40000 ALTER TABLE `note_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_api_relation`
--

DROP TABLE IF EXISTS `role_api_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_api_relation` (
  `id` char(32) NOT NULL COMMENT '主键ID',
  `role_id` char(32) NOT NULL COMMENT '角色ID',
  `api_pattern_id` char(32) NOT NULL COMMENT 'api 匹配模式ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_api_relation`
--

LOCK TABLES `role_api_relation` WRITE;
/*!40000 ALTER TABLE `role_api_relation` DISABLE KEYS */;
INSERT INTO `role_api_relation` VALUES ('15eb3c08aad511edb999e6aca8456320','15cca31232a011ed9b978c164579534f','0853de0be54a082d25db3c341c2471c4'),('182f625343df11ed9b978c164579534f','15cca31232a011ed9b978c164579534f','ea5e6e30de8efac0e08d077d7980200c'),('43235e9f43dd11ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','11e0403f305911ed9b978c164579534f'),('4324f52243dd11ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','11e0403f305911ed9b978c164579534f'),('5b2db3c2449211ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','d87728a68286cb6783565950a79fb182'),('5b2eebf6449211ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','d87728a68286cb6783565950a79fb182'),('5c2ab16a3a7911ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','1f01e10fac846ded52326dec3ddb30ed'),('5c2ce4f13a7911ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','1f01e10fac846ded52326dec3ddb30ed'),('7604640a39a711ed9b978c164579534f','15cca31232a011ed9b978c164579534f','ff6437b34f9aa7d8de868713a7d837e5'),('a659ef0e38e511ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','c6ecb164a40bc0cb469a73433a1e1042'),('aff5c35742e411ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','8e7ccbb8196df25c3a3c068a53cd8c66'),('aff6d1c742e411ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','8e7ccbb8196df25c3a3c068a53cd8c66'),('be1c1f883c0d11ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','432979c9321d757c79394b33cfe92919'),('be1f1f223c0d11ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','432979c9321d757c79394b33cfe92919'),('e4eb6a8a3c0c11ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','b5e84c52aa6e897df951945024fc9ef6'),('e4edbb443c0c11ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','b5e84c52aa6e897df951945024fc9ef6'),('f25d5ce03c0c11ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','72406b79d9c0ae065d029ae073258fa9'),('f260a89f3c0c11ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','72406b79d9c0ae065d029ae073258fa9'),('f8295dbf350111ed9b978c164579534f','15cca31232a011ed9b978c164579534f','a1895e68350111ed9b978c164579534f'),('fbe63d2a39a611ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','4be54faa0d2ece9206786861c4e81906'),('fbe8dcc839a611ed9b978c164579534f','0513aeb830a411ed9b978c164579534f','4be54faa0d2ece9206786861c4e81906');
/*!40000 ALTER TABLE `role_api_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user_relation`
--

DROP TABLE IF EXISTS `role_user_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_user_relation` (
  `id` char(32) NOT NULL COMMENT '关联id',
  `role_id` char(32) NOT NULL COMMENT '角色id',
  `user_id` char(32) DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user_relation`
--

LOCK TABLES `role_user_relation` WRITE;
/*!40000 ALTER TABLE `role_user_relation` DISABLE KEYS */;
INSERT INTO `role_user_relation` VALUES ('00ee4cbcaac811edb999e6aca8456320','0513aeb830a411ed9b978c164579534f',NULL),('00ee5e96aac811edb999e6aca8456320','3a71eabb8ff84fbb833977dc1dcf24f3',NULL),('08dd1864a46411ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','d0793ca178715c5492128cc6599f99a7'),('2b23e7f6a49411ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','8884873a372809eccdcbd4811f15e4ea'),('76f30c5ca49411ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','0da09bff1af8b0b3f455d590ae34edd9'),('791bd2b8aac811edb999e6aca8456320','0513aeb830a411ed9b978c164579534f',NULL),('791c2286aac811edb999e6aca8456320','3a71eabb8ff84fbb833977dc1dcf24f3',NULL),('7f2fefe0b368469da8341b69a8e5b3b6','0513aeb830a411ed9b978c164579534f','a1df6b81530b4055b9d20b86c7178f13'),('884133faaac811edb999e6aca8456320','0513aeb830a411ed9b978c164579534f','d660f3abc1637360e93a4c2dce18c06c'),('8841402aaac811edb999e6aca8456320','3a71eabb8ff84fbb833977dc1dcf24f3','d660f3abc1637360e93a4c2dce18c06c'),('8ed27c17449311ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','f38e882c540e8c9d65d2038902539871'),('9b929694a46311ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','fea53fd0adb86dab63425ecc940fefcb'),('a5781b2aa46311ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','70ef1956047a6abe072ae067bc52a36c'),('a5994112a49311ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','d465dac608ab81abef33712ee01eacb9'),('ba9cd2dea46311ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','4e109d74aa4eeeaae9725d0b23bb3e7e'),('e124a1f2a46311ed8d5112ea6021863e','3a71eabb8ff84fbb833977dc1dcf24f3','eead9837d09147a891c43f069761a1d3'),('e47dc7493a7b11ed9b978c164579534f','3a71eabb8ff84fbb833977dc1dcf24f3','52ffdec4d75c668ae3bb137eca912c9e');
/*!40000 ALTER TABLE `role_user_relation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-12 21:28:08

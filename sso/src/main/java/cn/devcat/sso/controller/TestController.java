package cn.devcat.sso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.devcat.sso.config.exception.DevcatUserException;
import cn.devcat.sso.config.log.Log;
import cn.devcat.sso.entity.vo.UserVO;
import cn.devcat.sso.service.IDevcatUserService;

@RestController
@RequestMapping("/test")
public class TestController {
	
	@Autowired
	IDevcatUserService userService;
	
	@Log("测试接口测试")
	@GetMapping("/hello")
	public String hello() {
		return "Hello World";
	}
	
	@GetMapping("/userVO")
	public UserVO getUserVO() {
		return userService.getUserVOByRequest();
	}
	
	@Log("异常测试接口")
	@GetMapping("/error")
	public void errorTest() throws DevcatUserException {
		throw new DevcatUserException("异常测试");
	}

}

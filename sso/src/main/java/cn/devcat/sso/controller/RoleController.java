package cn.devcat.sso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.devcat.sso.entity.DevcatRole;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatRoleService;
import cn.devcat.sso.util.RsUtil;

@RestController
@RequestMapping("/role")
public class RoleController {
	
	@Autowired
	IDevcatRoleService roleService;
	
	@PostMapping("/add")
	public Rs addRole(@RequestBody DevcatRole role) {
		roleService.addDevcatRole(role);
		return RsUtil.success("新增成功");
	}
	
	@DeleteMapping("/del")
	public Rs delRole(@RequestBody List<String> ids) {
		roleService.removeDevcatRole(ids);
		return RsUtil.success("删除成功");
	}
	
	@PutMapping("/set")
	public Rs setRole(@RequestBody DevcatRole role) {
		roleService.updateDevcatRole(role);
		return RsUtil.success("更新成功");
	}
	
	@GetMapping("/list")
	public Rs list(SearchCommonParam param) {
		PageData<DevcatRole> listDevcatRole = roleService.listDevcatRole(param);
		return RsUtil.success("查询成功", listDevcatRole);
	}

}

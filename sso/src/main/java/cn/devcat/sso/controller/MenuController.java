package cn.devcat.sso.controller;

import cn.devcat.sso.entity.DevcatMenu;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatMenuService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    IDevcatMenuService menuService;

    @PostMapping
    public Rs addMenu(@RequestBody DevcatMenu menu){
        menuService.addMenu(menu);
        return RsUtil.success("增加成功");
    }

    @DeleteMapping
    public Rs delMenu(@RequestBody List<String> ids){
        menuService.delMenu(ids);
        return RsUtil.success("删除成功");
    }

    @PutMapping
    public Rs setMenu(@RequestBody DevcatMenu menu){
        menuService.setMenu(menu);
        return RsUtil.success("修改成功");
    }


    @GetMapping
    public Rs menuTree(){
        List<DevcatMenu> menuList = menuService.menuTree();
        return RsUtil.success("获取成功", menuList);
    }

    @GetMapping("/all")
    public Rs menuList(SearchCommonParam param){
        PageData<DevcatMenu> list = menuService.menuList(param);
        return RsUtil.success("查询成功",list);

    }
}

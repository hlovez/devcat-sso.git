package cn.devcat.sso.controller;

import cn.devcat.sso.entity.DevcatNote;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.service.IDevcatNoteService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/public")
public class OpenController {

    @Autowired
    IDevcatNoteService noteService;

    @GetMapping("/about")
    public Rs getAboutPage(){
        String aboutPage = noteService.getAboutPage();
        return RsUtil.success("获取成功", aboutPage);
    }
}

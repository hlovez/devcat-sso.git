package cn.devcat.sso.controller;

import cn.devcat.sso.config.exception.DevcatUserException;
import cn.devcat.sso.config.log.Log;
import cn.devcat.sso.entity.*;
import cn.devcat.sso.entity.vo.FindPasswordVO;
import cn.devcat.sso.entity.vo.NormalSetUser;
import cn.devcat.sso.entity.vo.UserVO;
import cn.devcat.sso.service.IDevcatUserInfoService;
import cn.devcat.sso.service.IDevcatUserService;
import cn.devcat.sso.util.RsUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RequestMapping("/user")
@RestController
public class UserController {

	@Autowired
	IDevcatUserService userService;

	@Autowired
	IDevcatUserInfoService userInfoService;

	@GetMapping("/getUserVO")
	public Rs getUserVO() {
		UserVO vo = userService.getUserVOByRequest();
		return RsUtil.success("获取成功", vo);
	}

  @Log("用户注册")
	@PostMapping("/register")
	public Rs register(@RequestBody DevcatUser user) {
		userService.registerUser(user);
		return RsUtil.success("注册成功");
	}

	@GetMapping("/list")
	public Rs getUserList(SearchCommonParam param) {
		PageData<DevcatUser> userList = userService.getUserList(param);
		return RsUtil.success("查询成功", userList);
	}

	@PostMapping("/option/{optionType}/{optionValue}")
	public Rs delUser(@PathVariable("optionType") String optionType, @PathVariable("optionValue") Boolean optionValue, @RequestBody List<String> ids) {
		userService.optionUser(optionType, optionValue, ids);
		return RsUtil.success("操作成功");
	}

	@PutMapping("/set")
	public Rs setUser(@RequestBody DevcatUser user) {
		userService.updateDevcatUser(user);
		return RsUtil.success("更新成功");
	}

	@PostMapping("/bindRole")
	public Rs bindRoleByUser(@RequestBody Map<String,List<String>> param) {
		List<String> userIds = param.get("userIds");
		List<String> roleIds = param.get("roleIds");
		userService.bindRoleListForUserBatch(userIds, roleIds);
		return RsUtil.success("绑定成功");
	}

	@GetMapping("/userInfoById")
	public Rs getUserInfoByUserId(String userId) {
		DevcatUserInfo userInfoByUserId = userService.getUserInfoByUserId(userId);
		return RsUtil.success("查询成功", userInfoByUserId);
	}

	@GetMapping("/userInfo")
	public Rs getUserInfo() {
		DevcatUserInfo userInfoByRequest = userService.getUserInfoByRequest();
		return RsUtil.success("查询成功", userInfoByRequest);
	}

	@PostMapping("/setAvatar")
	public Rs setAvatar(MultipartFile file) {
		userInfoService.updateAvatar(file);
		return RsUtil.success("更新成功");
	}

	@PutMapping("/setUserInfo")
	public Rs setUserInfo(@RequestBody DevcatUserInfo userInfo) {
		userInfoService.setUserInfo(userInfo);
		return RsUtil.success("更新成功");
	}

	@GetMapping(value = "/avatar", produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] getAvatarBlob() throws DevcatUserException {
		return userInfoService.getAvatar();
	}

	@GetMapping(value = "/avatarByUserId/{userId}", produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] getAvatarBlob(@PathVariable("userId") String userId) {
		return userInfoService.getAvatar(userId);
	}

	@PutMapping("/setUser")
	public Rs setUsername(@RequestBody NormalSetUser user) {
		userService.setUser(user);
		return RsUtil.success("更新成功");
	}

	@DeleteMapping("/remove")
	public Rs removeUser() {
		userService.removeUser();
		return RsUtil.success("注销成功");
	}

	@GetMapping("/enableEmail")
	public Rs enableEmail(String enableCode) {
		userService.enableEmail(enableCode);
		return RsUtil.success(StringUtils.isEmpty(enableCode)?"激活邮件已发送，请即使前往邮箱进行激活":"激活成功");
	}

	@PostMapping("/findPassword")
	public Rs findPassword(@RequestBody FindPasswordVO findPasswordVO) {
		userService.findPassword(findPasswordVO.getUsername(), findPasswordVO.getCode(), findPasswordVO.getResetPassword());
		return RsUtil.success(StringUtils.isEmpty(findPasswordVO.getCode())?"验证码已发送到您到邮箱，请查收":"验证成功");
	}
}

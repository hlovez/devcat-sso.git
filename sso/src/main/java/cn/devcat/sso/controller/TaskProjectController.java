package cn.devcat.sso.controller;

import cn.devcat.sso.entity.DevcatTaskProject;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatTaskProjectService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class TaskProjectController {

  @Autowired
  IDevcatTaskProjectService projectService;

  @PostMapping
  public Rs addProject(@RequestBody DevcatTaskProject project){
    projectService.addProject(project);
    return RsUtil.success("创建成功");
  }

  @DeleteMapping
  public Rs delProject(@RequestBody List<Long> ids) {
    projectService.delProject(ids);
    return RsUtil.success("删除成功");
  }

  @PutMapping
  public Rs setProject(@RequestBody DevcatTaskProject project){
    projectService.setProject(project);
    return RsUtil.success("修改成功");
  }

  @GetMapping
  public Rs getProjectList(SearchCommonParam param){
    PageData<DevcatTaskProject> project = projectService.getProject(param);
    return RsUtil.success("查询成功",project);
  }

}

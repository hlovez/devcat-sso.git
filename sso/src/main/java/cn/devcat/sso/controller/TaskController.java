package cn.devcat.sso.controller;

import cn.devcat.sso.entity.DevcatTask;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatTaskService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/task")
public class TaskController {

  @Autowired
  IDevcatTaskService taskService;

  @PostMapping
  public Rs addTask(@RequestBody DevcatTask task){
    taskService.addTask(task);
    return RsUtil.success("创建成功");
  }

  @DeleteMapping
  public Rs delTask(@RequestBody List<Long> ids){
    taskService.delTask(ids);
    return RsUtil.success("删除成功");
  }

  @PutMapping
  public Rs setTask(@RequestBody DevcatTask task){
    taskService.setTask(task);
    return RsUtil.success("修改成功");
  }

  @GetMapping
  public Rs getTaskList(SearchCommonParam param){
    PageData<DevcatTask> taskList = taskService.getTaskList(param);
    return RsUtil.success("查询成功",taskList);
  }

  @GetMapping("/statistics")
  public Rs getTaskTotal(){
    Map<String, Long> statistics = taskService.statistics();
    return RsUtil.success("获取成功", statistics);
  }

}

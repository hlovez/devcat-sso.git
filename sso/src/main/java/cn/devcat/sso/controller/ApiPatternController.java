package cn.devcat.sso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.devcat.sso.config.log.Log;
import cn.devcat.sso.entity.DevcatApiPattern;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatApiPatternService;
import cn.devcat.sso.util.RsUtil;

@RestController
@RequestMapping("/apiPattern")
public class ApiPatternController {

	@Autowired
	IDevcatApiPatternService apiService;

	@Log("增加API接口配置")
	@PostMapping("/add")
	public Rs addApiPattern(@RequestBody DevcatApiPattern apiPattern) {
		apiService.addApiPattern(apiPattern);
		return RsUtil.success("新增成功");
	}

  @Log("删除API接口配置")
	@DeleteMapping("/del")
	public Rs delApiPattern(@RequestBody List<String> ids) {
		apiService.removeApiPattern(ids);
		return RsUtil.success("删除成功");
	}

  @Log("设置API接口配置")
	@PutMapping("/set")
	public Rs setApiPattern(@RequestBody DevcatApiPattern apiPattern) {
		apiService.updateApiPattern(apiPattern);
		return RsUtil.success("更新成功");
	}

	@GetMapping("/list")
	public Rs listApiPattern(SearchCommonParam param) {
		PageData<DevcatApiPattern> listApiPattern = apiService.listApiPattern(param);
		return RsUtil.success("查询成功", listApiPattern);
	}

}


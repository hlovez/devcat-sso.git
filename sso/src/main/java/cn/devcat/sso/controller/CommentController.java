package cn.devcat.sso.controller;

import cn.devcat.sso.config.log.Log;
import cn.devcat.sso.entity.BlogComment;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.service.IBlogCommentService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comment")
public class CommentController {

  @Autowired
  IBlogCommentService commentService;

  @Log("添加评论")
  @PostMapping
  public Rs addComment(@RequestBody BlogComment comment){
    commentService.addComment(comment);
    return RsUtil.success("添加成功");
  }

  @Log("删除评论")
  @DeleteMapping
  public Rs delComment(@RequestBody Map<String,Long> param){
    commentService.delComment(param.get("id"));
    return RsUtil.success("删除成功");
  }

  @GetMapping
  public Rs getCommentList(Long noteId){
    List<BlogComment> commentList = commentService.getCommonListByNoteId(noteId);
    return RsUtil.success("查询成功", commentList);
  }

}

package cn.devcat.sso.controller;

import cn.devcat.sso.entity.DevcatNote;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatNoteService;
import cn.devcat.sso.util.RsUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/blog")
public class BlogController {

  @Autowired
  IDevcatNoteService noteService;

  @GetMapping
  public Rs getBlogList(SearchCommonParam param){
    PageData<DevcatNote> publicNotePageData = noteService.getPublicNotePageData(param);
    return RsUtil.success("获取成功",publicNotePageData);
  }

  @GetMapping("/count")
  public void addViewCount(Integer noteId){
    DevcatNote one = noteService.getOne(new QueryWrapper<DevcatNote>().select("view_count").eq("id", noteId));
    if(one != null){
      noteService.update(new UpdateWrapper<DevcatNote>().eq("id",noteId).set("view_count",one.getViewCount()+1));
    }else{
      noteService.update(new UpdateWrapper<DevcatNote>().eq("id",noteId).set("view_count",1));
    }
  }

}

package cn.devcat.sso.controller;

import cn.devcat.sso.entity.NoteTag;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.INoteTagService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tag")
public class NoteTagController {

  @Autowired
  INoteTagService noteTagService;

  @PostMapping
  public Rs addTag(@RequestBody NoteTag noteTag){
    noteTagService.addTag(noteTag);
    return RsUtil.success("添加成功");
  }

  @DeleteMapping
  public Rs delTag(@RequestBody List<Long> ids){
    noteTagService.delTag(ids);
    return RsUtil.success("删除成功");
  }

  @PutMapping
  public Rs setTag(@RequestBody NoteTag tag){
    noteTagService.setTag(tag);
    return RsUtil.success("修改标签");
  }

  @GetMapping
  public Rs getTagList(SearchCommonParam param){
    PageData<NoteTag> tagList = noteTagService.tagList(param);
    return RsUtil.success("查询成功",tagList);
  }

}

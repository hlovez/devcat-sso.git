package cn.devcat.sso.controller;

import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Photo;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IPhotoService;
import cn.devcat.sso.util.RsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/photo")
public class PhotoController {

  @Autowired
  IPhotoService photoService;


  @PostMapping
  public Rs upload(MultipartFile file, String photoName, String photoDescribe){
    photoService.uploadPhoto(file, photoName, photoDescribe);
    return RsUtil.success("上传成功");
  }

  @GetMapping
  public Rs list(SearchCommonParam param){
    PageData<Photo> photoList = photoService.getPhotoList(param);
    return RsUtil.success("查询成功",photoList);
  }

  @DeleteMapping
  public Rs del(@RequestBody List<Long> ids){
    photoService.delPhoto(ids);
    return RsUtil.success("删除成功");
  }

}

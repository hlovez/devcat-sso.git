package cn.devcat.sso.controller;

import cn.devcat.sso.config.message.SSEServer;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.util.RsUtil;
import cn.devcat.sso.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@RequestMapping("/sse")
public class SSEController {

  @Autowired
  UserUtil userUtil;

  /**
   * 获取SSE连接
   */
  @GetMapping
  public SseEmitter connect(){
    String userId = userUtil.getUserByRequest().getId();
    return SSEServer.connect(userId);
  }

  @PostMapping
  public Rs sendMessage(String message){
    SSEServer.batchSendMessage("send by server: "+message);
    return RsUtil.success("发送成功");
  }

}

package cn.devcat.sso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.devcat.sso.entity.DevcatSystemLog;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.service.IDevcatSystemLogService;
import cn.devcat.sso.util.RsUtil;

@RestController
@RequestMapping("/log")
public class LogController {
	
	@Autowired
	IDevcatSystemLogService logService;
	
	@GetMapping("/list")
	public Rs getLogList(SearchCommonParam param) {
		PageData<DevcatSystemLog> devcatSystemLogList = logService.devcatSystemLogList(param);
		return RsUtil.success("查询成功", devcatSystemLogList);
	}
	
	@DeleteMapping("/del")
	public Rs delLog(@RequestBody List<String> ids) {
		logService.removeLog(ids);
		return RsUtil.success("删除成功");
	}

}

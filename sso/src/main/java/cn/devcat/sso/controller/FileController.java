package cn.devcat.sso.controller;

import cn.devcat.sso.entity.FileStore;
import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.service.IFileStoreService;
import cn.devcat.sso.util.JWTUtil;
import cn.devcat.sso.util.RedisUtil;
import cn.devcat.sso.util.RsUtil;
import cn.devcat.sso.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    IFileStoreService fileStoreService;

    @Autowired
    RedisUtil redisUtil;


    @PostMapping("/upload")
    public Rs upload(MultipartFile file){
        String fileId = fileStoreService.upload(file);
        return RsUtil.success("上传成功",fileId);
    }

    @GetMapping
    public Rs getFileList(String type, String keyword){
        List<FileStore> fileList = fileStoreService.getFileList(type, keyword);
        return RsUtil.success("获取成功",fileList);
    }


    @DeleteMapping
    public Rs delFile(@RequestBody List<String> ids){
        fileStoreService.delFile(ids);
        return RsUtil.success("删除成功");
    }

    @GetMapping("/download/{id}")
    public void download(@PathVariable("id") String id, HttpServletResponse response){
        try {
            fileStoreService.download(id, response);
        } catch (IOException e) {
            throw new RuntimeException("文件下载失败:"+e.getMessage());
        }
    }


    @PostMapping("/blog")
    public String uploadImageForBlog(MultipartFile file) {
        return fileStoreService.uploadImageForBlog(file);
    }
}

package cn.devcat.sso.entity;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@TableName
public class DevcatUser implements UserDetails {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	@TableId(type = IdType.ASSIGN_UUID)
	@Setter @Getter
	private String id;

	/**
	 * 用户名
	 */
	@Setter
	private String username;

	/**
	 * 密码
	 */
	@Setter
	private String password;

	/**
	 * 账号是否没有过期
	 */
	@Setter
	private Boolean accountNonExpired;

	/**
	 * 账号是否没有锁定
	 */
	@Setter
	private Boolean accountNonLocked;

	/**
	 * 密码是否没有过期
	 */
	@Setter
	private Boolean credentialsNonExpired;

	/**
	 * 是否启用用户
	 */
	@Setter
	private Boolean enabled;

	@Setter @Getter
	private String email;

	/**
	 * 邮箱验证状态
	 */
	@Setter @Getter
	private Boolean emailStatus;

	/**
	 * 用户创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Setter @Getter
	private LocalDateTime createTime;

	@JsonIgnore
	@Setter
	@TableField(exist = false)
	private Collection<? extends GrantedAuthority> authorities;

	@Setter @Getter
	@TableField(exist = false)
	private List<DevcatRole> roleList;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return accountNonExpired == null || accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return accountNonLocked == null || accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return credentialsNonExpired == null || credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enabled == null || enabled;
	}

}

package cn.devcat.sso.entity.vo;

import lombok.Data;

@Data
public class ApiInterfaceStatistics {

	/**
	 * API名称或路径
	 */
	private String apiName;
	
	/**
	 * 请求总次数
	 */
	private Long reqTotal;
	
	/**
	 * 平均请求时间
	 */
	private Integer avgReqTime;
}

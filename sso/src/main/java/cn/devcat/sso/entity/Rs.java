package cn.devcat.sso.entity;

import lombok.Data;

/**
 * 统一返回结果类
 * @author huhailong
 *
 */
@Data
public class Rs {
	private boolean status;
	private int statusCode;
	private String msg;
	private Object data;
}

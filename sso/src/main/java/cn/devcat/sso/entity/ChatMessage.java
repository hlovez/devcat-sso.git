package cn.devcat.sso.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName
public class ChatMessage {

  private String id;

  private String content;

  private String fromUserId;

  private String sendUserId;

  private Boolean isRead;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createTime;

}

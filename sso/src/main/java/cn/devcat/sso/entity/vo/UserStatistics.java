package cn.devcat.sso.entity.vo;

import lombok.Data;

@Data
public class UserStatistics {

	/**
	 * 用户总数
	 */
	private Long userTotal;
}

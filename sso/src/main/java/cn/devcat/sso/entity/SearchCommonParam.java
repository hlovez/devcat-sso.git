package cn.devcat.sso.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class SearchCommonParam implements Serializable {

    private static final long serialVersionUID = 7166516916470093975L;

    private Integer pageNum;

    private Integer pageSize;

    private String keyword;

    private String taskStatusType;

    private String userId;

    private String noteStatus;

    private Long noteId;

    private Long tagId;


}

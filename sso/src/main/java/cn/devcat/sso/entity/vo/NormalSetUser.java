package cn.devcat.sso.entity.vo;

import lombok.Data;

@Data
public class NormalSetUser {

	private String username;
	private String email;
	private String oldPassword;
	private String newPassword;
}

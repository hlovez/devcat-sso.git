package cn.devcat.sso.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class DevcatRole implements Serializable {

	private static final long serialVersionUID = 17248073958060236L;

	/**
	 * 角色id
	 */
	@TableId(type = IdType.ASSIGN_UUID)
	private String id;

	/**
	 * 角色代码
	 */
	private String roleCode;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 角色权重
	 */
	private Integer roleWeight;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;

}

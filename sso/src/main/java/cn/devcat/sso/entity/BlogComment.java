package cn.devcat.sso.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName
public class BlogComment {

  /**
   * ID 主键
   */
  @TableId(type = IdType.AUTO)
  private Long id;

  /**
   * 是否为主评论
   */
  private Boolean isMain;

  /**
   * 评论内容
   */
  private String content;

  /**
   * 主评论用户ID
   */
  private String mainUserId;

  @TableField(exist = false)
  private String mainUsername;

  @TableField(exist = false)
  private String mainUserAvatar;

  /**
   * 回复的用户
   */
  private String subUserId;

  @TableField(exist = false)
  private String subUsername;

  @TableField(exist = false)
  private String subUserAvatar;

  /**
   * 博客ID
   */
  private Long noteId;

  /**
   * 主评论ID
   */
  private Long mainCommentId;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime createTime;

  @TableField(exist = false)
  private List<BlogComment> subCommentList;

}

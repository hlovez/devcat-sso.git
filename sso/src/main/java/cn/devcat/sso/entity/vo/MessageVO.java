package cn.devcat.sso.entity.vo;

import lombok.Data;

@Data
public class MessageVO {
  private String type;
}

package cn.devcat.sso.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@TableName
public class DevcatSystemLog {

	/**
	 * 主键ID
	 */
	@TableId(type = IdType.ASSIGN_UUID)
	private String id;
	
	/**
	 * 日志描述
	 */
	private String logDescribe;
	
	/**
	 * 日志接口
	 */
	private String logApi;
	
	/**
	 * 访问用户，默认匿名者
	 */
	private String reqUsername;
	
	/**
	 * 请求参数
	 */
	private String reqParam;
	
	/**
	 * 请求的IP地址
	 */
	private String ipAddress;
	
	/**
	 * IP地址位置
	 */
	private String ipAddressLocal;
	
	/**
	 * 请求耗时
	 */
	private Long reqTime;
	
	/**
	 * 请求状态
	 */
	private Boolean reqStatus;
	
	/**
	 * 如果状态为false的时，该字段用于存储异常描述
	 */
	private String errorInfo;
	
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;
}

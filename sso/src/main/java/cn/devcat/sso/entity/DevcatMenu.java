package cn.devcat.sso.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@TableName
public class DevcatMenu {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String menuName;

    private String menuRoute;

    private Integer menuSort;

    private String parentId;

    private String menuRole;

    @TableField(exist = false)
    private List<String> roleList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField(exist = false)
    private List<DevcatMenu> subMenuList;
}

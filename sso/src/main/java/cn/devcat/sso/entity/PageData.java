package cn.devcat.sso.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class PageData<T> implements Serializable {

	private Long total;
	private List<T> list;

}

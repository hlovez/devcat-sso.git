package cn.devcat.sso.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@TableName
@Data
public class DevcatApiPattern implements Serializable {

	private static final long serialVersionUID = 5583895442273717156L;

	/**
	 * 主键
	 */
	@TableId(type  = IdType.ASSIGN_UUID)
	private String id;

	/**
	 * URL 匹配模式规则
	 */
	private String apiPattern;

	/**
	 * URL 描述
	 */
	private String apiDescribe;

	/**
	 * 是否启用改规则
	 */
	private Boolean enabled;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;

	/**
	 * 对应该API匹配模式下的角色列表
	 */
	@TableField(exist = false)
	private List<DevcatRole> roleList;


}

package cn.devcat.sso.entity.vo;

import lombok.Data;

@Data
public class FindPasswordVO {

	private String username;
	private String code;
	private String resetPassword;
}

package cn.devcat.sso.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class DevcatNote implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String noteTitle;

    private String coverImage;

    private String noteDescribe;

    private String userId;

    private String noteType;

    private String viewStatus;

    private String viewType;

    private Integer viewCount = 0;

    @TableField(exist = false)
    private Integer commentCount;

    @TableField(exist = false)
    private String nickName;

    @TableField(exist = false)
    private List<NoteTag> noteTagList;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}

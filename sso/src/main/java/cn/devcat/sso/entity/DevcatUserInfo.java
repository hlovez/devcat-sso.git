package cn.devcat.sso.entity;

import java.time.LocalDate;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@TableName
public class DevcatUserInfo {
	
	private String id;
	
	/**
	 * 昵称
	 */
	private String nickName;
	
	/**
	 * 性别
	 */
	private Integer gender;
	
	/**
	 * 年龄
	 */
	private Integer age;
	
	/**
	 * 头像
	 */
	@JsonIgnore
	private String avatar;
	
	/**
	 * 生日
	 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthday;
	
	/**
	 * 用户名
	 */
	private String username;
	
	@TableField(exist = false)
	private String email;
	
	@TableField(exist = false)
	private Boolean emailStatus;

}

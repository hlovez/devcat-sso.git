package cn.devcat.sso.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("stb_photos")
public class Photo {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String photoName;

    private String photoDescribe;

    private Boolean isPublic;

    private String uploadUserId;

    private String imagePath;

    @TableField(exist = false)
    private String username;

    private LocalDateTime createTime;
}

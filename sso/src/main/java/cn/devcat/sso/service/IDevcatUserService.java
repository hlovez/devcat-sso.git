package cn.devcat.sso.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.devcat.sso.config.exception.DevcatUserException;
import cn.devcat.sso.entity.DevcatUser;
import cn.devcat.sso.entity.DevcatUserInfo;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.entity.vo.NormalSetUser;
import cn.devcat.sso.entity.vo.UserVO;

public interface IDevcatUserService extends IService<DevcatUser> {

	DevcatUser getUserByUsername(String username);
	
	void setUser(NormalSetUser user) throws DevcatUserException;
	
	void removeUser();

	void registerUser(DevcatUser user) throws DevcatUserException;
	
	UserVO getUserVOByRequest();
	
	void optionUser(String optionType, Boolean optionValue, List<String> ids);
	
	void updateDevcatUser(DevcatUser devcatUser) throws DevcatUserException;
	
	PageData<DevcatUser> getUserList(SearchCommonParam param);
	
	void bindRoleListForUserBatch(List<String> userIds, List<String> roleIds);
	
	DevcatUserInfo getUserInfoByUserId(String userId);
	
	DevcatUserInfo getUserInfoByRequest();
	
	void enableEmail(String enableCode);
	
	void findPassword(String username, String code, String resetPassword);

}

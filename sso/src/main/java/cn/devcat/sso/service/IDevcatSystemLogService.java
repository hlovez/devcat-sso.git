package cn.devcat.sso.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.devcat.sso.entity.DevcatSystemLog;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;

public interface IDevcatSystemLogService extends IService<DevcatSystemLog> {
	
	PageData<DevcatSystemLog> devcatSystemLogList(SearchCommonParam param);
	
	void removeLog(List<String> ids);
}

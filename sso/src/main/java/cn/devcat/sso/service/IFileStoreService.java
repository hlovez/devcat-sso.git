package cn.devcat.sso.service;

import cn.devcat.sso.entity.FileStore;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IFileStoreService extends IService<FileStore> {

    String upload(MultipartFile file);

    List<FileStore> getFileList(String type, String keyword);

    void delFile(List<String> ids);

    void download(String fileId, HttpServletResponse response) throws IOException;

    String uploadImageForBlog(MultipartFile file);

}

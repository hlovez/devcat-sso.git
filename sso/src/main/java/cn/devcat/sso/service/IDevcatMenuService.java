package cn.devcat.sso.service;

import cn.devcat.sso.entity.DevcatMenu;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IDevcatMenuService extends IService<DevcatMenu> {

    void addMenu(DevcatMenu menu);

    void delMenu(List<String> ids);

    void setMenu(DevcatMenu menu);

    List<DevcatMenu> menuTree();

    PageData<DevcatMenu> menuList(SearchCommonParam param);

}

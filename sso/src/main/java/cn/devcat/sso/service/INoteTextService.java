package cn.devcat.sso.service;

import cn.devcat.sso.entity.NoteText;
import com.baomidou.mybatisplus.extension.service.IService;

public interface INoteTextService extends IService<NoteText> {

    void addNoteText(NoteText text);

    String getNoteText(Long id);

    void refreshCoverImage();
}

package cn.devcat.sso.service;

import cn.devcat.sso.entity.DevcatNote;
import cn.devcat.sso.entity.NoteText;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IDevcatNoteService extends IService<DevcatNote> {

    Long addNote(DevcatNote note);

    void delNote(List<Long> ids, boolean isAdmin);

    void setNote(DevcatNote note);

    void changeViewType(DevcatNote note);

    PageData<DevcatNote> getNotePageData(SearchCommonParam param);
    PageData<DevcatNote> getPublicNotePageData(SearchCommonParam param);

    String getAboutPage();

    DevcatNote getNoteById(Long id);

    PageData<DevcatNote> getNotePageDataForAdmin(SearchCommonParam param);

}

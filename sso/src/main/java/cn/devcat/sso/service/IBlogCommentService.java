package cn.devcat.sso.service;

import cn.devcat.sso.entity.BlogComment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IBlogCommentService extends IService<BlogComment> {

  void addComment(BlogComment comment);

  void delComment(Long id);

  List<BlogComment> getCommonListByNoteId(Long id);

}

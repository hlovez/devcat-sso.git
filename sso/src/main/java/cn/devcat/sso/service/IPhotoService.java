package cn.devcat.sso.service;

import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.Photo;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IPhotoService extends IService<Photo> {

  PageData<Photo> getPhotoList(SearchCommonParam param);

  void uploadPhoto(MultipartFile file, String photoName, String photoDescribe);

  void delPhoto(List<Long> ids);

}

package cn.devcat.sso.service;

import cn.devcat.sso.entity.DevcatTaskProject;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IDevcatTaskProjectService extends IService<DevcatTaskProject> {

  void addProject(DevcatTaskProject project);

  void delProject(List<Long> ids);

  void setProject(DevcatTaskProject project);

  PageData<DevcatTaskProject> getProject(SearchCommonParam param);

}

package cn.devcat.sso.service;

import cn.devcat.sso.entity.ChatMessage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IChatMessageService extends IService<ChatMessage> {

  List<ChatMessage> getChatMessageList (String chatUserId);

}

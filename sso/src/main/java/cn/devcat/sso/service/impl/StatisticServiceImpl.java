package cn.devcat.sso.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.devcat.sso.entity.vo.ApiInterfaceStatistics;
import cn.devcat.sso.entity.vo.UserStatistics;
import cn.devcat.sso.service.IDevcatUserService;
import cn.devcat.sso.service.IStatisticService;

@Service
public class StatisticServiceImpl implements IStatisticService {
	
	@Autowired
	IDevcatUserService userService;

	/**
	 * 用户统计接口
	 */
	@Override
	public UserStatistics userStatistics() {
		UserStatistics userStatistics = new UserStatistics();
		long userTotal = userService.count();
		userStatistics.setUserTotal(userTotal);
		return userStatistics;
	}

	/**
	 * API接口统计
	 */
	@Override
	public ApiInterfaceStatistics apiInterfaceStatistics() {
		ApiInterfaceStatistics apiInterfaceStatistics = new ApiInterfaceStatistics();
		
		return apiInterfaceStatistics;
	}

	
}

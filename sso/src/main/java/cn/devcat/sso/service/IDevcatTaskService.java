package cn.devcat.sso.service;

import cn.devcat.sso.entity.DevcatTask;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface IDevcatTaskService extends IService<DevcatTask> {

  void addTask(DevcatTask task);

  void delTask(List<Long> ids);

  void delTaskByProjectIds(List<Long> ids);

  void setTask(DevcatTask task);

  PageData<DevcatTask> getTaskList(SearchCommonParam param);

  Map<String,Long> statistics();

}

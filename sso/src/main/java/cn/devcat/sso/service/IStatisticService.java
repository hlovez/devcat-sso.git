package cn.devcat.sso.service;

import cn.devcat.sso.entity.vo.ApiInterfaceStatistics;
import cn.devcat.sso.entity.vo.UserStatistics;

public interface IStatisticService {

	UserStatistics userStatistics();
	
	ApiInterfaceStatistics apiInterfaceStatistics();
}

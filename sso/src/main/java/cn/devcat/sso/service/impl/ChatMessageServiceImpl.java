package cn.devcat.sso.service.impl;

import cn.devcat.sso.entity.ChatMessage;
import cn.devcat.sso.mapper.ChatMessageMapper;
import cn.devcat.sso.service.IChatMessageService;
import cn.devcat.sso.util.UserUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatMessageServiceImpl extends ServiceImpl<ChatMessageMapper, ChatMessage> implements IChatMessageService {

  @Autowired
  UserUtil userUtil;

  @Override
  public List<ChatMessage> getChatMessageList(String chatUserId) {
    return list(new QueryWrapper<ChatMessage>().eq("from_user_id", chatUserId)
      .eq("send_user_id", userUtil.getUserByRequest().getId()).orderByAsc("create_time"));
  }

}

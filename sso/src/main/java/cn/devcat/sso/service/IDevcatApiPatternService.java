package cn.devcat.sso.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.devcat.sso.entity.DevcatApiPattern;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;

public interface IDevcatApiPatternService extends IService<DevcatApiPattern> {
	
	List<DevcatApiPattern> getAllApiParrentList();

	void addApiPattern(DevcatApiPattern apiPattern);
	
	void bindApiParrentAndRole(String apiParrentId, String roleId);
	
	void removeApiPattern(List<String> ids);
	
	void updateApiPattern(DevcatApiPattern apiPattern);
	
	PageData<DevcatApiPattern> listApiPattern(SearchCommonParam param);
	
}

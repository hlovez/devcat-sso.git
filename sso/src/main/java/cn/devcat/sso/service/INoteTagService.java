package cn.devcat.sso.service;

import cn.devcat.sso.entity.NoteTag;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface INoteTagService extends IService<NoteTag> {

    void addTag(NoteTag noteTag);

    void delTag(List<Long> ids);

    void setTag(NoteTag noteTag);

    PageData<NoteTag> tagList(SearchCommonParam param);

    void bindTagRelation(Long noteId, List<NoteTag> tagList);

    void unbindTagRelation(List<Long> ids, String type);

    List<NoteTag> getTagListByNoteId(Long noteId);
}

package cn.devcat.sso.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageInfo;

import cn.devcat.sso.entity.DevcatSystemLog;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;
import cn.devcat.sso.mapper.DevcatSystemLogMapper;
import cn.devcat.sso.service.IDevcatSystemLogService;
import cn.devcat.sso.util.PageHelperUtil;

@Service
public class DevcatSystemLogServiceImpl extends ServiceImpl<DevcatSystemLogMapper, DevcatSystemLog> implements IDevcatSystemLogService {

	@Override
	public PageData<DevcatSystemLog> devcatSystemLogList(SearchCommonParam param) {
		QueryWrapper<DevcatSystemLog> wrapper = new QueryWrapper<>();
		if(StringUtils.isNotEmpty(param.getKeyword())) {
			wrapper.like("log_describe", param.getKeyword()).or().like("log_api", param.getKeyword());
		}
		wrapper.orderByDesc("create_time");
		PageHelperUtil.handle(param);
		List<DevcatSystemLog> list = list(wrapper);
		return PageHelperUtil.makePageData(new PageInfo<DevcatSystemLog>(list));
	}

	@Override
	public void removeLog(List<String> ids) {
		removeBatchByIds(ids);
	}

	
}

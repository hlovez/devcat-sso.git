package cn.devcat.sso.service;

import javax.mail.MessagingException;

import cn.devcat.sso.entity.DevcatUser;

public interface ISendMailService {
	
	void sendMail(DevcatUser userByRequest) throws MessagingException;

}

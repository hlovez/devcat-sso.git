package cn.devcat.sso.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.devcat.sso.entity.DevcatRole;
import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;

public interface IDevcatRoleService extends IService<DevcatRole> {

	List<DevcatRole> getRoleListByUserId(String userId);

	List<DevcatRole> getRoleListByApiPatternId(String apiPatternId);

	void addDevcatRole(DevcatRole role);

	void removeDevcatRole(List<String> ids);

	void updateDevcatRole(DevcatRole role);

	PageData<DevcatRole> listDevcatRole(SearchCommonParam param);

}

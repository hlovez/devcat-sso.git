package cn.devcat.sso.service;

import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.devcat.sso.config.exception.DevcatUserException;
import cn.devcat.sso.entity.DevcatUser;
import cn.devcat.sso.entity.DevcatUserInfo;

public interface IDevcatUserInfoService extends IService<DevcatUserInfo> {

	void initUserByRegister(DevcatUser user);
	
	void updateAvatar(MultipartFile file) throws DevcatUserException;
	
	void setUserInfo(DevcatUserInfo userInfo) throws DevcatUserException;
	
	byte[] getAvatar() throws DevcatUserException;
	
	byte[] getAvatar(String userId) throws DevcatUserException;
}

package cn.devcat.sso.enums;

public enum EmailTypeEnum {
	
	REGISTER_MAILL("enableMail"),
	FORGETPASSWORD_MAIL("findPassword"),
	;
	
	private String sendMailType;
	
	EmailTypeEnum(String sendMailType){
		this.sendMailType = sendMailType;
	}
	
	public String typeName() {
		return sendMailType;
	}

}

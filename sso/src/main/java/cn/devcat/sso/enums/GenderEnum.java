package cn.devcat.sso.enums;

/**
 * 性别枚举
 * @author huhailong
 *
 */
public enum GenderEnum {

	MALE(0, "男"),
	FEMALE(1, "女"),
	SECRET(3, "保密");
	
	/**
	 * 代码
	 */
	private final Integer code;
	
	/**
	 * 含义
	 */
	private final String meaing;
	
	GenderEnum(Integer code, String meaing){
		this.code = code;
		this.meaing = meaing;
	}
	
	public Integer code() {
		return code;
	}
	
	public String meaing() {
		return meaing;
	}
	
	public static GenderEnum getEnumByCode(String meaing) {
		for(GenderEnum gender : values()) {
			if(gender.meaing.equals(meaing)) {
				return gender;
			}
		}
		return null;
	}
}

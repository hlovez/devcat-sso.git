package cn.devcat.sso.enums;

public enum ViewTypeEnum {

    PRIVATE("private"),
    PUBLIC("public"),
    ABOUT("about");

    private String type;

    ViewTypeEnum(String type){
        this.type = type;
    }

    public static ViewTypeEnum typeEnum(String type){
        for (ViewTypeEnum value : values()) {
            if(type.equals(value.type)){
                return value;
            }
        }
        throw new RuntimeException("未找到对应笔记类型");
    }

    public String getType(){
        return type;
    }
}

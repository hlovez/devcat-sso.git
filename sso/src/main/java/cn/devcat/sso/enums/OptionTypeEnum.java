package cn.devcat.sso.enums;

/**
 * 操作用户枚举
 * @author huhailong
 *
 */
public enum OptionTypeEnum {

	REMOVE("remove"),
	ACCOUNT_LOCK("lock"),
	ACCOUNT_EXPIRE("account_expire"),
	PASSWORD_EXPIRE("passwd_expire"),
	ENABLE("enable"),
  ENABLE_EMAIL("enable_email")
	;

	private String optionType;

	OptionTypeEnum(String optionType){
		this.optionType = optionType;
	}

	public static OptionTypeEnum getOptionTypeEnum(String optionType) {
		for(OptionTypeEnum typeEnum : values()) {
			if(typeEnum.optionType.equals(optionType)) {
				return typeEnum;
			}
		}
		return null;
	}
}

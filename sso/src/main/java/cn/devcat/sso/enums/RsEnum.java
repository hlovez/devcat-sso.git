package cn.devcat.sso.enums;

/**
 * 响应枚举类
 * @author huhailong
 *
 */
public enum RsEnum {

	SUCCESS 		(10000),	//请求成功
	SYSTEM_WARNING	(10001),	//系统警告
	SYSTEM_ERROR 	(10002),	//系统错误
	NON_LOGIN 		(10003),	//未登录错误
	NON_AUTHEN 		(10004),	//登录失败错误
	NON_AUTHOR 		(10006);	//权限校验错误
	
	private int code;
	
	RsEnum(int code){
		this.code = code;
	}
	
	public int code() {
		return code;
	}
}

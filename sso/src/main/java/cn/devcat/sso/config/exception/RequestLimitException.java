package cn.devcat.sso.config.exception;

import org.springframework.security.core.AuthenticationException;

@SuppressWarnings("serial")
public class RequestLimitException extends AuthenticationException {
	
	public RequestLimitException(String message) {
		super(message);
	}

}

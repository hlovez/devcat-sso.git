package cn.devcat.sso.config.exception;

@SuppressWarnings("serial")
public class DevcatUserException extends RuntimeException{
	
	public DevcatUserException(String message) {
		super(message);
	}

}

package cn.devcat.sso.config.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import cn.devcat.sso.util.HttpUtil;
import cn.devcat.sso.util.RedisUtil;
import cn.devcat.sso.util.RsUtil;

/**
 * 退出成功处理
 * @author huhailong
 *
 */
@Component
public class DevcatLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	RedisUtil redisUtil;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		String uuid = request.getHeader(HttpHeaders.AUTHORIZATION);
		if(redisUtil.checkKey(uuid)) {
			redisUtil.delete(uuid);
		}
		HttpUtil.sendResponse(response, RsUtil.success("已安全退出"));
	}

}

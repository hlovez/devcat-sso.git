package cn.devcat.sso.config.security;

import javax.servlet.http.HttpServletResponse;

import cn.devcat.sso.entity.DevcatUser;
import cn.devcat.sso.enums.RsEnum;
import cn.devcat.sso.util.HttpUtil;
import cn.devcat.sso.util.RsUtil;

public class SecurityMessageUtil {

	public static final String LOGIN_SUCCESS = "登录成功";
	public static final String NO_LOGIN = "请登录后重试";
	public static final String NO_ACCESS = "当前权限无法访问，请联系管理员";
	public static final String TOKEN_ERROR = "鉴权失败，请重新登录";
	public static final String ACCOUNT_EXPIRE = "账户已过期";
	public static final String ACCOUNT_LOCKED = "账户已锁定";
	public static final String PASSWD_EXPIRE = "密码已过期";
	public static final String ACCOUNT_NO_ENABLED = "账号已失效";
	public static final String BAD_CALL = "您涉嫌非法请求，已被拦截";

	public static void checkUserStatus(DevcatUser user, HttpServletResponse response) {
		if (!user.isAccountNonExpired()) {
			HttpUtil.sendResponse(response, RsUtil.error(ACCOUNT_EXPIRE, RsEnum.NON_AUTHEN));
			return;
		}
		if (!user.isAccountNonLocked()) {
			HttpUtil.sendResponse(response, RsUtil.error(ACCOUNT_LOCKED, RsEnum.NON_AUTHEN));
			return;
		}
		if (!user.isCredentialsNonExpired()) {
			HttpUtil.sendResponse(response, RsUtil.error(PASSWD_EXPIRE, RsEnum.NON_AUTHEN));
			return;
		}
		if (!user.isEnabled()) {
			HttpUtil.sendResponse(response, RsUtil.error(ACCOUNT_NO_ENABLED, RsEnum.NON_AUTHEN));
			return;
		}
	}
}

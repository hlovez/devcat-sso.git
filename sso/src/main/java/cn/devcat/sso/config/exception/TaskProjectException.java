package cn.devcat.sso.config.exception;

public class TaskProjectException extends RuntimeException{

  public TaskProjectException(String message){
    super(message);
  }
}

package cn.devcat.sso.config.exception;

@SuppressWarnings("serial")
public class DevcatApiPatternException extends RuntimeException {

	public DevcatApiPatternException(String message) {
		super(message);
	}
}

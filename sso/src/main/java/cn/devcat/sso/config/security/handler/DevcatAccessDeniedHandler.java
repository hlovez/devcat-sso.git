package cn.devcat.sso.config.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import cn.devcat.sso.config.security.SecurityMessageUtil;
import cn.devcat.sso.enums.RsEnum;
import cn.devcat.sso.util.HttpUtil;
import cn.devcat.sso.util.RsUtil;

public class DevcatAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		HttpUtil.sendResponse(response, RsUtil.error(SecurityMessageUtil.NO_ACCESS, RsEnum.NON_AUTHOR));
	}
	
	

}

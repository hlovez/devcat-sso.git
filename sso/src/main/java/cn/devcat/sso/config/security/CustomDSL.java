package cn.devcat.sso.config.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import cn.devcat.sso.config.security.filter.DevcatAuthenticationFilter;
import cn.devcat.sso.config.security.handler.DevcatLoginFailHandler;
import cn.devcat.sso.config.security.handler.DevcatLoginSuccessHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomDSL extends AbstractHttpConfigurer<CustomDSL, HttpSecurity> {
	
	private DevcatLoginSuccessHandler devcatLoginSuccessHandler;
	
	public CustomDSL(DevcatLoginSuccessHandler devcatLoginSuccessHandler) {
		this.devcatLoginSuccessHandler = devcatLoginSuccessHandler;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		log.info("come in custom dsl");
		AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
		DevcatAuthenticationFilter filter = new DevcatAuthenticationFilter(authenticationManager);
		filter.setAuthenticationSuccessHandler(devcatLoginSuccessHandler);
		filter.setAuthenticationFailureHandler(new DevcatLoginFailHandler());
		http.addFilterAt(filter, UsernamePasswordAuthenticationFilter.class);
	}
	
	public static CustomDSL customDsl(DevcatLoginSuccessHandler devcatLoginSuccessHandler) {
        return new CustomDSL(devcatLoginSuccessHandler);
    }
	
}

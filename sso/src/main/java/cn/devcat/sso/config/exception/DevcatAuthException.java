package cn.devcat.sso.config.exception;

import org.springframework.security.core.AuthenticationException;

@SuppressWarnings("serial")
public class DevcatAuthException extends AuthenticationException{

	public DevcatAuthException(String msg) {
		super(msg);
	}

}

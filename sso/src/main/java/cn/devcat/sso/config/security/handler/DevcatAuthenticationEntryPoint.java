package cn.devcat.sso.config.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import cn.devcat.sso.config.security.SecurityMessageUtil;
import cn.devcat.sso.enums.RsEnum;
import cn.devcat.sso.util.HttpUtil;
import cn.devcat.sso.util.RsUtil;

public class DevcatAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		HttpUtil.sendResponse(response, RsUtil.error(SecurityMessageUtil.NO_LOGIN, RsEnum.NON_LOGIN));
	}
	
	

}

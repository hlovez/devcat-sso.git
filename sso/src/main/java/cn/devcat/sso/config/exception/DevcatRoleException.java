package cn.devcat.sso.config.exception;

@SuppressWarnings("serial")
public class DevcatRoleException extends RuntimeException {

	public DevcatRoleException(String message) {
		super(message);
	}
}

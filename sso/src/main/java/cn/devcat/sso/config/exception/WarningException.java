package cn.devcat.sso.config.exception;

/**
 * 警告异常
 * @author huhailong
 *
 */
@SuppressWarnings("serial")
public class WarningException extends RuntimeException {

	public WarningException(String warningMessage) {
		super(warningMessage);
	}
}

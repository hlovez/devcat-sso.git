package cn.devcat.sso.config.security.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import cn.devcat.sso.enums.RsEnum;
import cn.devcat.sso.util.HttpUtil;
import cn.devcat.sso.util.RsUtil;

public class DevcatLoginFailHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		HttpUtil.sendResponse(response, RsUtil.error(exception.getLocalizedMessage(), RsEnum.NON_AUTHEN));
		
	}

}

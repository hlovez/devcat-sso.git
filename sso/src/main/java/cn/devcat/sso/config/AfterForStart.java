package cn.devcat.sso.config;

import cn.devcat.sso.util.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AfterForStart implements ApplicationRunner {

  @Autowired
  RedisUtil redisUtil;

  @Override
  public void run(ApplicationArguments args) throws Exception {
    redisUtil.clearCache();
    log.info("clear cache successful!");
  }
}

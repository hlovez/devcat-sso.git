package cn.devcat.sso.config.exception;

@SuppressWarnings("serial")
public class SendMailException extends RuntimeException {
	
	public SendMailException (String errorMessage) {
		super(errorMessage);
	}

}

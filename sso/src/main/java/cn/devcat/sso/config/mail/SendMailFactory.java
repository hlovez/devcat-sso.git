package cn.devcat.sso.config.mail;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.devcat.sso.config.exception.SendMailException;
import cn.devcat.sso.enums.EmailTypeEnum;
import cn.devcat.sso.service.ISendMailService;

/**
 * 发送邮件工厂
 * @author huhailong
 *
 */
@Component
public class SendMailFactory {
	
	private final Map<String, ISendMailService> sendMailMap;
	
	@Autowired
	public SendMailFactory(Map<String,ISendMailService> sendMailMap) {
		this.sendMailMap = sendMailMap;
	}
	
	public ISendMailService createSendMailService(EmailTypeEnum typeEnum) throws SendMailException {
		String sendEmailType = typeEnum.typeName();
		ISendMailService sendMailService = sendMailMap.get(sendEmailType);
		if(sendMailService==null) {
			throw new SendMailException("没有匹配到对应到邮件模版，请联系管理员处理");
		}
		return sendMailService;
	}

}

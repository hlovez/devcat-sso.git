package cn.devcat.sso.config.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.enums.RsEnum;
import cn.devcat.sso.util.RsUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 全局异常处理
 * 
 * @author huhailong
 *
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(WarningException.class)
	public Rs warningHandler(WarningException e) {
		return RsUtil.warning(e.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public Rs exceptionHandler(Exception e) {
		e.printStackTrace();
		log.error("global exception handler:{}",e.getMessage());
		return RsUtil.error(e.getMessage(), RsEnum.SYSTEM_ERROR);
	}
	
	@ExceptionHandler(DevcatAuthException.class)
	public Rs authExceptionHandler(DevcatAuthException e) {
		return RsUtil.error(e.getMessage(), RsEnum.NON_LOGIN);
	}
}

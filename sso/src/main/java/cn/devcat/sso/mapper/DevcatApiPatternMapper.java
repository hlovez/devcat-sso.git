package cn.devcat.sso.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.devcat.sso.entity.DevcatApiPattern;

public interface DevcatApiPatternMapper extends BaseMapper<DevcatApiPattern> {

	List<DevcatApiPattern> getApiPatternListByRoleId(@Param("roleId") String roleId);
	
	void bindApiPatternAndRole(@Param("apiPatternId") String apiParrentId, @Param("roleId") String roleId);
	
	void unBindApiPatternAndRole(@Param("ids") List<String> ids);
}

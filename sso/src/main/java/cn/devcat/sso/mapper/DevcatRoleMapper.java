package cn.devcat.sso.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.devcat.sso.entity.DevcatRole;

public interface DevcatRoleMapper extends BaseMapper<DevcatRole> {

	List<DevcatRole> getRoleListByUserId(@Param("userId") String userId);

	List<DevcatRole> getRoleListByApiPatternId(@Param("apiPatternId") String apiPatternId);

	void clearRoleRelationForUser(@Param("roleIds") List<String> roleIds);

	void clearRoleRelationForApiPattern(@Param("roleIds") List<String> roleIds);

}

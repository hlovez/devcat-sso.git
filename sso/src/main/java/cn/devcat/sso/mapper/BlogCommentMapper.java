package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.BlogComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BlogCommentMapper extends BaseMapper<BlogComment> {

  List<BlogComment> getCommentListByNoteId(@Param("noteId") Long noteId);

}

package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.Photo;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PhotoMapper extends BaseMapper<Photo> {

  List<Photo> getPhotoList(@Param("pojo")SearchCommonParam param);

}

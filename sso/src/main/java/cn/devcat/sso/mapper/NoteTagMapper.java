package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.NoteTag;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NoteTagMapper extends BaseMapper<NoteTag> {

    void bindTagRelation(@Param("noteId") Long noteId, @Param("tagIdList") List<Long> tagIdList);

    void unbindRelation(@Param("id") Long id, @Param("type") String type);

    List<NoteTag> getNoteTagList(@Param("pojo")SearchCommonParam param);

}

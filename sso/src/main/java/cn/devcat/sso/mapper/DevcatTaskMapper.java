package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.DevcatTask;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DevcatTaskMapper extends BaseMapper<DevcatTask> {

  List<DevcatTask> getTaskList(@Param("pojo")SearchCommonParam pojo);

}

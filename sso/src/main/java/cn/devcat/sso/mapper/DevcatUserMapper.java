package cn.devcat.sso.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.devcat.sso.entity.DevcatUser;

/**
 * mapper 映射接口
 * 
 * @author huhailong
 *
 */
public interface DevcatUserMapper extends BaseMapper<DevcatUser> {

	void bindRoleListForUser(@Param("userId") String userId, @Param("roleIds") List<String> roleIds);
	
	void clearRoleUserRelationByUserId(@Param("userIds") List<String> userIds);
}

package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.ChatMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ChatMessageMapper extends BaseMapper<ChatMessage> {
}

package cn.devcat.sso.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.devcat.sso.entity.DevcatSystemLog;

public interface DevcatSystemLogMapper extends BaseMapper<DevcatSystemLog> {

}

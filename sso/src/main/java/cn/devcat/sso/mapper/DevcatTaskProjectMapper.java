package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.DevcatTaskProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DevcatTaskProjectMapper extends BaseMapper<DevcatTaskProject> {
}

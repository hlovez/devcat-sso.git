package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.FileStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface FileStoreMapper extends BaseMapper<FileStore> {
}

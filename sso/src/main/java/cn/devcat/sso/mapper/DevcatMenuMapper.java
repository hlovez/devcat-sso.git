package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.DevcatMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface DevcatMenuMapper extends BaseMapper<DevcatMenu> {
}

package cn.devcat.sso.mapper;

import cn.devcat.sso.entity.DevcatNote;
import cn.devcat.sso.entity.SearchCommonParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DevcatNoteMapper extends BaseMapper<DevcatNote> {

  List<DevcatNote> getDevcatNoteList(@Param("pojo")SearchCommonParam pojo, @Param("userId") String userId);

  DevcatNote getNoteById(@Param("id") Long id);

}

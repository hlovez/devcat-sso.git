package cn.devcat.sso.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.devcat.sso.entity.DevcatUserInfo;

public interface DevcatUserInfoMapper extends BaseMapper<DevcatUserInfo> {

}

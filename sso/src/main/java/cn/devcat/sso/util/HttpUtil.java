package cn.devcat.sso.util;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;

import com.alibaba.fastjson2.JSONObject;

import cn.devcat.sso.entity.Rs;
import lombok.extern.slf4j.Slf4j;

/**
 * Http 工具类
 */
@Slf4j
public class HttpUtil {

    /**
     * 发送响应
     */
    public static void sendResponse(HttpServletResponse response, Rs rs){
        //设置response的相关属性，为跨域做准备
//        response.setHeader("Access-Control-Allow-Origin", request.getHeader(HttpHeaders.ORIGIN));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        //允许请求方式
        response.setHeader("Access-Control-Allow-Methods", "POST,PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        //需要放行header头部字段 如需鉴权字段，自行添加，如Authorization
        response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS,"devcat-token");
        response.setHeader("Access-Control-Allow-Headers", "content-type,x-requested-with,devcat-token,Authorization,authorization");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        OutputStream out = null;
        try {
            out = response.getOutputStream();
            out.write(JSONObject.toJSONString(rs).getBytes());
            out.flush();
            out.close();
        } catch (IOException e) {
            log.info("出现异常"+e.getMessage());
        }finally {
            if(out != null){
                try{
                    out.close();
                }catch (Exception e){
                    log.error(e.getMessage());
                }
            }
        }
    }
    /**
     * 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址;
     */
    public static String getIpAddress(HttpServletRequest request) {
        // 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址

        String ip = request.getHeader("X-Forwarded-For");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }
        } else if (ip.length() > 15) {
            String[] ips = ip.split(",");
            for (String s : ips) {
                if (!("unknown".equalsIgnoreCase(s))) {
                    ip = s;
                    break;
                }
            }
        }
        return ip;
    }

}

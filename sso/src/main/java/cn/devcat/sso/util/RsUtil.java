package cn.devcat.sso.util;

import cn.devcat.sso.entity.Rs;
import cn.devcat.sso.enums.RsEnum;

/**
 * RS工具类
 * @author huhailong
 *
 */
public class RsUtil {

	/**
	 * 请求成功，无返回数据
	 * @param message 成功提示信息
	 */
	public static Rs success(String message) {
		Rs rs = new Rs();
		rs.setStatus(true);
		rs.setStatusCode(RsEnum.SUCCESS.code());
		rs.setMsg(message);
		return rs;
	}
	
	/**
	 * 请求成功，并返回数据
	 * @param message 成功提示信息
	 * @param data 携带的数据
	 */
	public static Rs success(String message, Object data) {
		Rs rs = new Rs();
		rs.setStatus(true);
		rs.setStatusCode(RsEnum.SUCCESS.code());
		rs.setMsg(message);
		rs.setData(data);
		return rs;
	}
	
	/**
	 * 请求失败
	 * @param message 报错提示信息
	 */
	public static Rs error(String message, RsEnum errType) {
		Rs rs = new Rs();
		rs.setStatus(false);
		rs.setStatusCode(errType.code());
		rs.setMsg(message);
		return rs;
	}
	
	/**
	 * 请求失败, 并携带数据
	 * @param message 报错提示信息
	 * @param data 携带的数据
	 */
	public static Rs error(String message, Object data, RsEnum errType) {
		Rs rs = new Rs();
		rs.setStatus(false);
		rs.setStatusCode(errType.code());
		rs.setMsg(message);
		rs.setData(data);
		return rs;
	}
	
	/**
	 * 警告
	 * @param message 警告信息
	 */
	public static Rs warning(String message) {
		Rs rs = new Rs();
		rs.setStatus(false);
		rs.setStatusCode(RsEnum.SYSTEM_WARNING.code());
		rs.setMsg(message);
		return rs;
	}
	
	/**
	 * 警告，并携带数据
	 * @param message 警告信息
	 */
	public static Rs warning(String message, Object data) {
		Rs rs = new Rs();
		rs.setStatus(false);
		rs.setStatusCode(RsEnum.SYSTEM_WARNING.code());
		rs.setMsg(message);
		rs.setData(data);
		return rs;
	}
}

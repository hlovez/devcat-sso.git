package cn.devcat.sso.util;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.devcat.sso.entity.PageData;
import cn.devcat.sso.entity.SearchCommonParam;

/**
 * 分页工具类
 * @author 19768
 *
 */
public class PageHelperUtil {
	
	public static void handle(SearchCommonParam param) {
		if(param.getPageNum()!=null&&param.getPageSize()!=null) {
			PageHelper.startPage(param.getPageNum(), param.getPageSize());
		}
	}

	public static <T> PageData<T> makePageData(PageInfo<T> pageInfo){
		PageData<T> pageData = new PageData<T>();
		pageData.setTotal(pageInfo.getTotal());
		pageData.setList(pageInfo.getList());
		return pageData;
	}
}

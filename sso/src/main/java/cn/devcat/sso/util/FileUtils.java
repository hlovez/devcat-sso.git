package cn.devcat.sso.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 文件工具类
 * @author 胡海龙
 */
public class FileUtils {

    /**
     * 获取文件名后缀
     * @param fileName 文件全名称
     * @return 后缀名
     */
    public static String suffix(String fileName){
        if(fileName!=null&&fileName.contains(".")){
            String[]splits = fileName.split("\\.");
            return "."+splits[splits.length-1];
        }
        return "";
    }

    public static void mkdir(String dir){
        try {
            Path path = Paths.get(dir);

            if(!Files.exists(path)){
                Files.createDirectory(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("创建文件夹失败:"+dir+":"+e.getMessage());
        }
    }

    public static void delFile(String filePath){
        try {
            Files.deleteIfExists(Paths.get(filePath));
        } catch (IOException e) {
            throw new RuntimeException("删除文件失败");
        }
    }
}
